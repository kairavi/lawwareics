let express = require('express');
let app = express();
const ics = require('ics')
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const { writeFileSync } = require('fs')

app.listen(3000);

app.post('/createExportFile', function(req, res) {
  let event = req.body.data

  let new_event = []
  if(event) {
    for(let i = 0; i < event.length; i++) {
      console.log("event[i]", event[i]);
      event[i]['start'] = convertTime(event[i]['date'].split(' ')[1]);
      event[i]['date'] = convertDate(event[i]['date'].split(' ')[0]);

      let events = []
      events['title'] = event[i]['event'] + ' - ' + event[i]['first'] + ' ' + event[i]['last'] + ' vs ' + event[i]['defendant'] + ', Venue: ' + event[i]['venue']
      events['description'] = 'Case number: ' + event[i]['caseno'] + ', Judge: ' + event[i]['judge'] + ', Attorney Responsible: ' + event[i]['attyass'] + ', Attorney Handling: ' + event[i]['attyhand'] + ', Notes: ' + event[i]['notes']
      events['location'] = event[i]['location']
      events['start'] = [event[i]['date'][0], event[i]['date'][1], event[i]['date'][2], event[i]['start'][0], event[i]['start'][1]]
      events['duration'] = {minutes: event[i]['tottime']}
      new_event.push(events);
    }
    const { error, value } = ics.createEvents(new_event)

    if (error) {
      res.send("error").end()
    }else{
        writeFileSync(`../lawware/cal_event.ics`, value);
        res.send("success").end()
    }
  }
});

  function convertDate(date) {
    let newDate = []
    let splitStartDate = date.split("-")
    newDate[0] = parseInt(splitStartDate[0])
    newDate[1] = parseInt(splitStartDate[1])
    newDate[2] = parseInt(splitStartDate[2])
    return newDate
  }

  function convertTime(startTime) {
    let splitStartTime = startTime.split(":")
    if((splitStartTime[1] != 00) && ((splitStartTime[1] - 30) < 0)) {
      splitStartTime[0] = splitStartTime[0] - 6
      splitStartTime[1] = +60 - +splitStartTime[1]
    } else if(splitStartTime[1] == 00) {
      splitStartTime[0] = splitStartTime[0] - 6
      splitStartTime[1] = 30
    } else {
      splitStartTime[0] = splitStartTime[0] - 5
      splitStartTime[1] = splitStartTime[1] - 30
    }
    return splitStartTime
  }